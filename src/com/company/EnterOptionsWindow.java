package com.company;

import com.company.game.AppWindow;
import com.company.game.ClientSocketAdapter;
import com.company.game.ServerSocketAdaper;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EnterOptionsWindow extends JFrame{
    private JPanel panel;
    private JButton startClientButton;
    private JButton startServerButton;
    private JTextField textField1;
    private JButton startButton;
    private JLabel ipLabel;

    public EnterOptionsWindow(){
        startButton.setVisible(false);
        ipLabel.setVisible(false);
        textField1.setVisible(false);
        textField1.setText("127.0.0.1");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Options");
        add(panel);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
        startClientButton.addActionListener(actionEvent -> {
            startButton.setVisible(true);
            ipLabel.setVisible(true);
            textField1.setVisible(true);
            pack();
            setLocationRelativeTo(null);
        });
        startButton.addActionListener(actionEvent -> {
            new AppWindow();
            new Thread(new ClientSocketAdapter(textField1.getText())).start();
            dispose();
        });
        startServerButton.addActionListener(actionEvent -> {
            new AppWindow();
            new Thread(new ServerSocketAdaper()).start();
            dispose();
        });
        textField1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                textField1.setText("");
            }
        });
    }

}
