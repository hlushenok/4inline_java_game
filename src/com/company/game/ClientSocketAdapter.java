package com.company.game;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClientSocketAdapter implements Runnable {
    String ip;
    public ClientSocketAdapter(String ip) {
        this.ip = ip;
    }

    @Override
    public void run() {
            openSocket();
    }
    private void openSocket() {
        try {
            Socket socket = new Socket(ip, 8889);
            new Thread(new GetArray(socket)).start();
            new Thread(new SendArray(socket)).start();
        }catch (Exception e){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            System.err.println("Server connection failed. reconnect...");
            openSocket();
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
    class GetArray extends Thread{
        Socket socket;

        GetArray(Socket socket){
            this.socket= socket;
        }

        @Override
        public void run() {
            super.run();
            while (true) {
                try {
                    Thread.sleep(500);
                    ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                    Object array  = inputStream.readObject();
                    Field.secondPlayerPoints = ((ArrayList<int[]>) array);
                }catch (Exception e){
                    openSocket();
                }
            }
        }
    }
    class SendArray extends Thread{
        Socket socket;
        SendArray(Socket socket) throws Exception {
            this.socket=socket;
        }
        @Override
        public void run() {
            super.run();
            while (true) {
                try {
                    Thread.sleep(500);
                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject(Field.playerPoints);
                    outputStream.flush();
                }catch (Exception e){
                    openSocket();
                }

            }
        }
    }
}
