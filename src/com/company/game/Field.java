package com.company.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class Field extends JPanel implements ActionListener{
    private int fieldCount = 24;
    private Dimension windowSize;
    private ArrayList<int[]> fieldsList;
    static ArrayList<int[]> playerPoints;
    static ArrayList<int[]> secondPlayerPoints;
    static int xSize ;
    static int ySize ;
    private Image fieldsImg;
    private Image yourPoint;
    private Image yourTurn;
    private Image enemyPoint;
    private Image youWin;
    private Image notToday;
    boolean inGame = true;
    boolean firstPlayerStarts = false;
    boolean secondPlayerStarts = false;
    boolean firstPlayerWin = false;
    boolean secondPlayerWin = false;
    private Timer timer = new Timer(250,this);
    private static MouseEvent mouseEvent;

    public Field(Dimension windowSize){
        this.windowSize = windowSize;
        fieldsList = getFields();
        playerPoints = new ArrayList<>();
        secondPlayerPoints = new ArrayList<>();
        setBackground(Color.LIGHT_GRAY);
        addMouseListener(new MouseActionsListener());
        setFocusable(true);
        timer.start();
        loadImages();
    }

    private void loadImages(){
        ImageIcon fi = new ImageIcon("field.png");
        fieldsImg = fi.getImage();
        ImageIcon yp = new ImageIcon("yourPoint.png");
        yourPoint = yp.getImage();
        ImageIcon yt = new ImageIcon("yourTurn.png");
        yourTurn = yt.getImage();
        ImageIcon ep = new ImageIcon("enemyPoint.png");
        enemyPoint = ep.getImage();
        ImageIcon yw = new ImageIcon("youWin.png");
        youWin= yw.getImage();
        ImageIcon nt = new ImageIcon("notToday.png");
        notToday = nt.getImage();
    }

    ArrayList<int[]> getFields(){

        ArrayList<int[]> fieldsList = new ArrayList<>();
        xSize = windowSize.height/fieldCount;
        ySize = windowSize.width/fieldCount;
        for (int i = 0; i<fieldCount-1; i++){
            for (int k = 0; k<fieldCount+1; k++){
                fieldsList.add(new int[]{k*xSize,i*ySize});
            }
        }
        return fieldsList;
    }

    private void createPoint(int x, int y, ArrayList<int[]> playerArray, ArrayList<int[]> secondPlayerPoints){

        int betterX = -1;
        int betterY = -1;
        for (int i = 0; i < fieldsList.size() ; i++) {

            if (fieldsList.get(i)[0]+xSize-x<=xSize && betterX<fieldsList.get(i)[0]){
                    betterX = fieldsList.get(i)[0];
            }
            if (fieldsList.get(i)[1]+ySize-y<=ySize && betterY<fieldsList.get(i)[1]){
                    betterY = fieldsList.get(i)[1];
            }
        }
        if (checkCollision(betterX,betterY,playerArray,secondPlayerPoints) && isMyTurn()) {
            playerArray.add(new int[]{betterX, betterY});
        }
        if (!new LineFinder().checkPoints(fieldsList,playerArray,fieldCount)){
            inGame = false;
            firstPlayerWin = true;
        }
        if (!new LineFinder().checkPoints(fieldsList,secondPlayerPoints,fieldCount)){
            inGame = false;
            secondPlayerWin = true;
        }
    }

    private boolean isMyTurn(){
        if (!firstPlayerStarts && !secondPlayerStarts){
            return true;
        }
        if (firstPlayerStarts && playerPoints.size()==secondPlayerPoints.size()){
            return true;
        }
        if (secondPlayerStarts && playerPoints.size()<secondPlayerPoints.size()){
            return true;
        }
        mouseEvent=null;
        return false;
    }

    private void whoFirst(){
        if (playerPoints.size()>0&&secondPlayerPoints.size()==0){
            firstPlayerStarts=true;
        }
        if (playerPoints.size()==0&&secondPlayerPoints.size()>0){
            secondPlayerStarts=true;
        }
    }

    private boolean checkCollision(int x,int y, ArrayList<int[]> playerArray, ArrayList<int[]> secondPlayerPoints) {
        if (!inGame){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            playerPoints = new ArrayList<>();
            secondPlayerWin=false;
            secondPlayerStarts=false;
            firstPlayerStarts=false;
            firstPlayerWin=false;
            mouseEvent = null;
            inGame = true;
            return false;
        }
        for (int i = 0; i < playerArray.size() ; i++) {
            if (playerArray.get(i)[0]==x && playerArray.get(i)[1]==y){
                return false;
            }
        }
        for (int i = 0; i < secondPlayerPoints.size() ; i++) {
            if (secondPlayerPoints.get(i)[0]==x && secondPlayerPoints.get(i)[1]==y){
                return false;
            }
        }
        return true;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
            for (int i = 0; i < fieldsList.size(); i++) {
                graphics.drawImage(fieldsImg,fieldsList.get(i)[0], fieldsList.get(i)[1], xSize, ySize, this);
            }
            drawPlayerPoints(graphics, playerPoints,secondPlayerPoints);
        if (firstPlayerWin){
            graphics.drawImage(youWin,fieldCount*xSize/2-150,fieldCount*ySize/2-150,300,300,this);

        }
        if (secondPlayerWin){
            graphics.drawImage(notToday,fieldCount*xSize/2-150,fieldCount*ySize/2-150,300,300,this);
        }

    }

    private void drawPlayerPoints(Graphics graphics, ArrayList<int[]> playerArray,ArrayList<int[]> secondPlayerPoints) {
        for (int i = 0; i < playerArray.size(); i++) {
            if (isMyTurn()){
                graphics.drawImage(yourTurn,playerArray.get(i)[0], playerArray.get(i)[1], xSize, ySize,this);
            }else{
                graphics.drawImage(yourPoint,playerArray.get(i)[0], playerArray.get(i)[1], xSize, ySize,this);
            }

        }
        for (int i = 0; i < secondPlayerPoints.size(); i++) {
            graphics.setColor(Color.BLACK);
            graphics.drawImage(enemyPoint,secondPlayerPoints.get(i)[0], secondPlayerPoints.get(i)[1], xSize, ySize, this);
        }
        repaint();
    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (playerPoints.size()==0 || secondPlayerPoints.size()==0){
            whoFirst();
        }
        if (mouseEvent!=null) {
            createPoint(mouseEvent.getX(), mouseEvent.getY(), playerPoints, secondPlayerPoints);
        }
        repaint();

    }


    class MouseActionsListener extends MouseAdapter{
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            super.mouseClicked(mouseEvent);
            Field.mouseEvent = mouseEvent;
        }
    }

}
