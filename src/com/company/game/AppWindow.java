package com.company.game;

import javax.swing.*;
import java.awt.*;

public class AppWindow extends JFrame{
    private Dimension windowSize = new Dimension(666,666);

    public AppWindow(){
        setVisible(true);
        setMinimumSize(windowSize);
        setTitle("Six In Line");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        add(new Field(windowSize));
    }
}
