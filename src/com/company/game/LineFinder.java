package com.company.game;

import java.util.ArrayList;

public class LineFinder{

    int LINE_SIZE = 3;

    public LineFinder(){

    }

    public boolean checkPoints(ArrayList<int[]> field, ArrayList<int[]> playerPoints,int fieldsCount) {
        int counterLeftRight = 0;
        int counterTopDown = 0;
        int counterTopLeftDownRight = 0;
        int counterDownLeftTopRight = 0;
        ArrayList<int[]> poinsOnField = new ArrayList<>();

        for (int i = 0; i <field.size() ; i++) {
            poinsOnField.add(new int[]{});
            for (int j = 0; j < playerPoints.size() ; j++) {
                if (field.get(i)[0]==playerPoints.get(j)[0] && field.get(i)[1]==playerPoints.get(j)[1]){
                    poinsOnField.set(i,new int[]{playerPoints.get(j)[0],playerPoints.get(j)[1]});
                }

            }
        }

        for (int i = poinsOnField.size()-1; i > 0 ; i--) {

            //Left Right
            if (poinsOnField.get(i).length > 0 && poinsOnField.get(i - 1).length > 0) {
                counterLeftRight++;
            }
            if (poinsOnField.get(i).length > 0 && poinsOnField.get(i - 1).length == 0 && counterLeftRight != 5) {
                counterLeftRight = 0;
            }
            if (counterLeftRight == LINE_SIZE) {
                return false;
            }

            //Top Down
            if (i > fieldsCount && poinsOnField.get(i).length > 0 && poinsOnField.get(i - fieldsCount - 1).length > 0) {
                counterTopDown++;
            }
            if (i > fieldsCount && i < poinsOnField.size()-fieldsCount && poinsOnField.get(i).length > 0 && poinsOnField.get(i - fieldsCount - 1).length == 0 && counterTopDown != 5 && poinsOnField.get(i + fieldsCount+1).length != 0) {
                counterTopDown = 0;
            }
            if (counterTopDown == LINE_SIZE) {
                return false;
            }

            //Top Left Down Right
            if (i > fieldsCount && poinsOnField.get(i).length > 0 && poinsOnField.get(i - fieldsCount).length > 0) {
                counterTopLeftDownRight++;
            }
            if (i > fieldsCount && i < poinsOnField.size()-fieldsCount && poinsOnField.get(i).length > 0 && poinsOnField.get(i - fieldsCount).length == 0 && counterTopLeftDownRight != 5 && poinsOnField.get(i + fieldsCount).length != 0) {
                counterTopLeftDownRight = 0;
            }
            if (counterTopLeftDownRight == LINE_SIZE) {
                return false;
            }

            //Down Left Top Right
            if (i > fieldsCount && poinsOnField.get(i).length > 0 && poinsOnField.get(i - fieldsCount-2).length > 0) {
                counterDownLeftTopRight++;
            }
            if (i > fieldsCount && i < poinsOnField.size()-fieldsCount && poinsOnField.get(i).length > 0 && poinsOnField.get(i - fieldsCount-2).length == 0 && counterDownLeftTopRight != 5 && poinsOnField.get(i + fieldsCount+2).length != 0) {
                counterDownLeftTopRight = 0;
            }
            if (counterDownLeftTopRight == LINE_SIZE) {
                return false;
            }

        }
        return true;
    }
}

