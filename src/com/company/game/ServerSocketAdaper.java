package com.company.game;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServerSocketAdaper implements Runnable {
    ServerSocket serverSocket;
    Socket socket;
    Thread send;
    Thread get;

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(8889);
        }catch (Exception e){
            e.printStackTrace();
        }
        openSocket();
    }

    private void openSocket() {
        try{
            socket = serverSocket.accept();
            if (socket.isConnected()) {
                Field.playerPoints = new ArrayList<>();
                send = new Thread(new SendArray(socket));
                send.start();
                get = new Thread(new GetArray(socket));
                get.start();
                System.err.println("Threads created !");
            }
        }catch (Exception e){

            openSocket();
        }
    }
    class GetArray extends Thread{
        Socket socket;
        GetArray(Socket socket){
            this.socket= socket;
        }
        @Override
        public void run() {
            super.run();
            while (true) {
                try {
                    Thread.sleep(500);
                    ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                    Object array  = inputStream.readObject();
                    Field.secondPlayerPoints = ((ArrayList<int[]>) array);
                }catch (Exception e){
                    System.out.println("some");
                    get.interrupt();
                    openSocket();
                }
            }
        }
    }
    class SendArray extends Thread{
        Socket socket;
        SendArray(Socket socket) throws Exception {
            this.socket=socket;
        }
        @Override
        public void run() {
            super.run();
            while (true) {
                try {
                    Thread.sleep(500);
                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject(Field.playerPoints);
                    outputStream.flush();
                }catch (Exception e){
                    System.out.println("some");
                    send.interrupt();
                    openSocket();
                }

            }
        }
    }
}
